from django.db import models
import datetime
from django.utils import timezone
# Create your models here.


class Member(models.Model):
    id_email = models.CharField(max_length=100)
    password = models.CharField(max_length=100)
    birthday = models.CharField(max_length=20, null=True)
    gender = models.CharField(max_length=10, null=True)
    country = models.CharField(max_length=100, null=True)
    photo = models.CharField(max_length=500, null=True)


    def __unicode__(self):
        data = self.id_email+"::"+self.password
        return data
