__author__ = 'moon'


from django.conf.urls import patterns, url
from memapi import views



urlpatterns = patterns('',
    url(r'^$', views.writeinfo, name ='writeinfo'),
    url(r'^sign_in$', views.sign_in, name ='sign_in'),
    url(r'^sign_out$', views.sign_out, name='sign_out'),
    url(r'^close$', views.close, name='close')

            )