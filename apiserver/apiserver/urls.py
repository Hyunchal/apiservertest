from django.conf.urls import patterns, include, url

from django.contrib import admin


admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'apiserver.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^api/v1/users/', include('memapi.urls', namespace="member")),
    url(r'^admin/', include(admin.site.urls)),
)
